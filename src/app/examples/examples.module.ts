import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgramCoursesComponent } from './components/program-courses/program-courses.component';
import { ExamplesRoutingModule } from './examples-routing.module';
import { SharedModule } from '@universis/common';
import { MostModule } from '@themost/angular';
import { AdvancedFormsModule } from '@universis/forms';
import { RulesModule } from '@universis/ngx-rules';
import { RouterModalModule } from '@universis/common/routing';
import { TablesModule } from '@universis/ngx-tables';

@NgModule({
  imports: [
    CommonModule,
    ExamplesRoutingModule,
    MostModule,
    AdvancedFormsModule,
    SharedModule,
    RouterModalModule,
    TablesModule,
    RulesModule
  ],
  declarations: [ProgramCoursesComponent]
})
export class ExamplesModule { }
