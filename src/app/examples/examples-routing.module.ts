import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';
import { AdvancedFormItemResolver, AdvancedFormRouteItemResolver } from '@universis/forms';
import { ItemRulesModalComponent, RuleFormModalData } from '@universis/ngx-rules';
import { SpecializationCourseResolver, StudyProgramResolver } from '../resolvers';

import { ProgramCoursesComponent } from './components/program-courses/program-courses.component';
const tableConfiguration = require('../../assets/tables/SpecializationCourse/list.json');

const routes: Routes = [
  {
    path: 'example1',
    component: ProgramCoursesComponent,
    data: {
      model: 'ProgramCourses',
      title: 'Program Course Requirements',
      tableConfiguration: tableConfiguration
    },
    children: [
      {
        path: 'studyPrograms/:studyProgram/courses/:id/rules',
        pathMatch: 'full',
        component: ItemRulesModalComponent,
        outlet: 'modal',
        data: <RuleFormModalData> {
          model: 'ProgramCourses',
          closeOnSubmit: true,
          serviceQueryParams: {
            $expand: 'course,program',
          },
          navigationProperty: 'RegistrationRules'
        },
        resolve: {
          data: AdvancedFormItemResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExamplesRoutingModule { }
