import { Component, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-program-courses',
  templateUrl: './program-courses.component.html',
  styleUrls: ['./program-courses.component.css']
})
export class ProgramCoursesComponent implements OnInit {

  constructor(private context: AngularDataContext) { }

  ngOnInit() {
    //
  }

}
