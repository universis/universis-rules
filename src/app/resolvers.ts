import { Injectable } from '@angular/core';
import { whenRendered } from '@angular/core/src/render3';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Injectable({
    providedIn: 'root'
})
export class StudyProgramResolver implements Resolve<any> {
  constructor(private context: AngularDataContext) { }
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any> | any {
    if (route.params.studyProgram) {
      return this.context.model(`StudyPrograms`)
          .where('id').equal(route.params.studyProgram)
          .getItem();
    }
    return null;
  }
}

@Injectable({
    providedIn: 'root'
})
export class SpecializationCourseResolver implements Resolve<any> {
  constructor(private context: AngularDataContext) { }
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any> | any {
    if (route.params.specializationCourse) {
      return this.context.model(`SpecializationCourses`)
          .where('id').equal(route.params.specializationCourse)
          .expand('studyProgram', 'course')
          .getItem();
    }
    return null;
  }
}