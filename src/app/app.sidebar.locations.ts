export const APP_SIDEBAR_LOCATIONS = [
  {
    name: 'Home',
    key: 'Sidebar.Dashboard',
    url: '/home',
    icon: 'fa fa-archive',
    index: 0
  },
  {
    name: 'Set requirements of a study program course',
    key: 'Sidebar.ProgramCourses',
    url: '/examples/example1',
    icon: 'fa fa-book',
    index: 0
  }
];
