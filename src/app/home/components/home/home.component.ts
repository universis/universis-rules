import { HttpClient, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Converter } from 'showdown';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  content: string;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get('assets/README.md', {
      responseType: 'text'
   }).toPromise().then(text => {
      this.content = new Converter().makeHtml(text);
    }, (err) => {
      console.error(err);
    })
  }

}
